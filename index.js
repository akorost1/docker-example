require('dotenv').config();

const express = require('express');
const compression = require('compression');
const httpContext = require('express-http-context');
const fs = require('fs');
const https = require('https');
const morgan = require('morgan');
const methodOverride = require('method-override');
const { createLogger, format, transports } = require('winston');
const path = require('path');
const redirectionMiddleware = require('./middleware/redirectionMiddleware');

const LOG_DIR = '/var/log/app/';
const LOG_FILE = process.env.LOG_FILE || 'website.log';

const options = {
  cert: fs.readFileSync('./cert/tls.crt'),
  key: fs.readFileSync('./cert/tls.key'),
};

const app = express();
app.use(compression());

const environment = process.env.NODE_ENV || 'Development';

const HTTP_PORT = process.env.HTTP_PORT || 9000;
const HTTPS_PORT = process.env.HTTPS_PORT || 9001;

const angularBuildPath = '/dist/website';

app.use(
  '/',
  // redirectionMiddleware,
  express.static(path.join(__dirname, angularBuildPath)),
);

app.use(morgan('dev'));
app.use(express.json());
app.use(express.json({ type: 'application/vnd.api+json' }));
app.use(methodOverride());
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});

app.get(['/favicon.ico', '/icon/favicon.ico'], (req, res) => {
  res.status(204);
});

app.post('/log', function (req, res) {
  try {
    const jsonObj = {
      SessionId: req.body.SessionId,
      EventType: req.body.Event,
      EventName: req.body.EventName,
      EventTime: req.body.EventTime,
    };
    logger.log('info', JSON.stringify(jsonObj));
    res.status(200);
  } catch {
    // Regardless of what happens, this log should return that we're OK
    res.status(200);
  }
});

app.get('/health', (req, res) => {
  res.jsonp({
    status: 'up',
    v: '3.12.1.2'
  });
});

/**
 * Return configuration JSON
 */
app.get('/config.json', (req, res) => {
  const filename = path.join(__dirname, `/config/config.json`);
  res.setHeader('Content-Type', 'application/json');
  // If there's a config file  -- try to read and output
  if (fs.existsSync(filename)) {
    fs.readFile(filename, 'utf8', (err, data) => {
      if (err) {
        res.status(500);
        res.jsonp({
          error: 'Cannot read configuration file',
        });
      } else {
        res.send(data);
      }
    });
  } else {
    // If there's no config  -- throw 404
    res.status(404);
    res.end();
  }
});

app.all('/*', function (req, res, next) {
  // Just send the index.html for other files to support HTML5Mode
  res.sendFile('index.html', { root: path.join(__dirname, angularBuildPath) });
});

// Add Correlation ID to log
const correlationId = format((info) => {
  info.correlationId = httpContext.get('correlationId');
  return info;
});

// Create file logger of max 50 MB
const logger = createLogger({
  level: 'info',
  format: format.combine(
    correlationId(),
    format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss',
    }),
    format.errors({ stack: true }),
    format.splat(),
    format.json(),
  ),
  transports: [
    new transports.File({
      filename: path.join(LOG_DIR, LOG_FILE),
      level: 'info',
      maxsize: 50000000,
    }),
  ],
});

//
// If we're not in production then **ALSO** log to the `console`
// with the colorized simple format.
//
if (process.env.NODE_ENV !== 'production') {
  logger.add(
    new transports.Console({
      format: format.combine(
        format.timestamp({
          format: 'YYYY-MM-DD HH:mm:ss',
        }),
        format.simple(),
      ),
    }),
  );
}

logger.log('info', 'Starting the log at %s', LOG_DIR + LOG_FILE);
app.listen(HTTP_PORT);
https.createServer(options, app).listen(HTTPS_PORT);
console.log(`Website app started { port: ${HTTP_PORT}, environment: ${environment} }`);
console.log(`Website app started { port: ${HTTPS_PORT}, environment: ${environment} }`);
