FROM node:16-alpine as build

WORKDIR /build
# Bring over all the source files to the working directory
COPY package.json index.js angular.json tsconfig.json yarn.lock .browserslistrc ./
COPY middleware ./middleware
# copy certificates
COPY ./cert/ ./cert/
# skip devDependencies
RUN yarn install
COPY config ./config
COPY src ./src

ARG NODE_ENV
ENV NODE_ENV ${NODE_ENV}
RUN $(npm bin)/ng build --configuration=${NODE_ENV}

# Copy from build to another container
FROM node:16-alpine
COPY --from=build /build /
EXPOSE 9000 9001

CMD yarn start
