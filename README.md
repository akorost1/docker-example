## Requirements

- Node.js & NPM installed
- [Yarn](https://yarnpkg.com/lang/en/docs/install/)
- Angular Cli installed globally (`npm install -g @angular/cli`)

[Yarn](https://yarnpkg.com/en/) is a package manager like NPM. Usage is different a little bit.

[Yarn cheatsheet](https://devhints.io/yarn).

## Running locally

```
yarn
yarn dev
```

## Running Unit Tests locally

```
ng test --browsers=Chrome --configuration=Testing
```

or

```
yarn test
```

Run `yarn install` (or just `yarn`) to make sure you have the latest packages.

If you add a new unit test, make sure to add it to the array in `/angular.json` projects → website → test → options → include

If you don't want to see the tests displayed in a browser, switch the option to `--browsers=ChromeHeadless`

## Environments/Configuration

We don't use `environment.ts` files anymore. Angular will read `config.json` file on start from `config` folder. This file should not be committed.

For local developmemnt copy `config.local.example.json` to `config/config.json`. Replace external endpoints, Okta config, Azure connections with the values from the Conducktor ConfigMap in DEV environment.

Use command `yarn dev`to serve the site and automatically reload on change. `yarn dev`is an alias for `ng serve --configuration=Local`.

## Cleaning up after adding/updating modules

After unpdating to Angular 13 the `yarn dev` command fails sometimes after updaing/adding/removing Node modules. To fix this delete the `.angular/cache` folder and re-run `yarn` and `yarn dev`.

## HTTPS/TLS

To run website on HTTPS we use self-signed certifcates.

### Local developmemnt

For the local development generate self-signed certificate and key, name them `tls.crt` and `tls.key` accordingly and put into `cert` directory.

### Conducktor environments

In Conducktor these self-signed certificates are stored in secret `website-tls`. Certificate is added to the container on the pod start using `secretMaps`.


## (Important) Feature Creation

All components, services, directives, and pipes(filters) must be created via the `angular-cli` (make sure you have the` @angular/cli` installed)

`g` stands for generate

Use `s` or `c` for service, component, etc.

See documentation: https://angular.io/cli/generate

```
ng g s /home/dashboard/dashboard-service # to generate a service in the dashboard

ng g c /home/dashboard/dashboard # to create a dashboard component in the dashboard
```

## Building the desired environment

By default, `ng serve` will start with the local environment. You can pass desired environment (`local`, `Development`, `Staging`, `Production`) with `--configuration` parameter. E.g.: `ng build --configuration=Production`

If no `env` is passed, angular will default to `local` environment.

## Passing `NODE_ENV`

If `NODE_ENV` is set to one of the following: `Development`, `Staging`, `Production`, `NODE_ENV` is used with Docker deployment or local Docker development.

```
docker build -t website . --build-arg NODE_ENV=Development
```

```
docker run -p 8080:9000 -d website
```

## ESLint Set Up

The linter requirements are initialized in package.json.

1. Run command yarn in the website root folder.
2. In VSCode, search extensions for "ESLint". Install ESLint
3. In VSCode, search extensions for "Prettier - Code formatter". Install "Prettier - Code formatter"
4. Restart your VSCode.

All rules should be highlighted and automatically fixed on document save.

